# Recipe App API Proxy 

NGINX Proxy App for our Recipe Application API 

## Usage 

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `API_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
